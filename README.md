# doomseeker-blobs

Storage repository for binary blob data used by Doomseeker such as IP2C

# GeoLite2

This product includes GeoLite2 data created by MaxMind,
available from http://www.maxmind.com.

Database and Contents Copyright (c) 2018 MaxMind, Inc.

GeoLite2 is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.

----

# This File

Copyright (C) 2018 The Doomseeker Team

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
